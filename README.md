# mit

The code to reproduce the results in the paper:

Beauregard, L. and Izzo, D. and Acciarini, G.: "Breaking traditions: introducing a surrogate Primer Vector in non Keplerian dynamics" - ISSFD 2024

# Install

We reccomend using a separate python environment to run *mit*. We provide an environment file in the repo:

``
mamba env create -f environment.yml mamba activate mit
``