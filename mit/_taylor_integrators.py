import heyoka as hey
import time
import numpy as np
import pickle as pkl

def _add_variational_equations(x, f):
    """Given some array containing the symbols and the rhs of a 6DOF dynamics, this
    funtion augments it adding the corresponding variational equations (with symbols phi_ij)

    Args:
        x (array (1,)): heyoka symbols
        f (array (1,)): heyoka expressions for the dynamics

    Returns:
        array: heyoka expression for the augmented dynamics
    """
    # We create the symbols for the state transition matrix (variational equations) phi_ij
    symbols_phi = []
    for i in range(6):
        for j in range(6):
            # Here we define the symbol for the variations
            symbols_phi.append("phi_" + str(i) + str(j))
    phi = np.array(hey.make_vars(*symbols_phi)).reshape((6, 6))

    # And compute the r.h.s of the variational equations
    # First dfdx
    dfdx = []
    for i in range(6):
        for j in range(6):
            # This is the symbolic expression for df_i/dx_j
            dfdx.append(hey.diff(f[i], x[j]))
    dfdx = np.array(dfdx).reshape((6, 6))
    # .. then the (variational) equations of motion
    dphidt = dfdx @ phi
    # We pack all in one dynamics
    dyn = []
    for state, rhs in zip(x, f):
        dyn.append((state, rhs))
    for state, rhs in zip(phi.reshape((36,)), dphidt.reshape((36,))):
        dyn.append((state, rhs))
    return dyn


def create_kepler_variational(
    symbols=["x", "y", "z", "vx", "vy", "vz"], tol=1e-18, filename=None
):
    """Builds the adaptive Taylor integrator for the Kepler problem
    adding the corresponding variational equations.

    NOTE: The gravitational parameter is represented as an heyoka parameter and thus can be changed
    from its default value here set to 1.

    Args:
        symbols (list, optional): The list of symbols for the state. Defaults to ["x", "y", "z", "vx", "vy", "vz"].
        tol (float, optional): The tolerance of the Taylor integrator. Defaults to 1e-16.
        filename (string, optional): If not None, willcreate the filename and pickle there the Taylor integrator.

    Returns:
        heyoka.taylor_adaptive: the numerical integrator
    """
    # Create the symbolic variables for the state.
    x = np.array(hey.make_vars(*symbols))
    # This will contain the r.h.s. of the equations
    dx = []
    # The equations of motion (pure Kepler).
    inv_r3 = (x[0]**2 + x[1]**2 + x[2]**2) ** (-1.5)
    dx.append(x[3])
    dx.append(x[4])
    dx.append(x[5])
    dx.append(-hey.par[0] * x[0] * inv_r3)
    dx.append(-hey.par[0] * x[1] * inv_r3)
    dx.append(-hey.par[0] * x[2] * inv_r3)
    dx = np.array(dx)

    # We augment the dynamics with the variational equations
    aug_dyn = _add_variational_equations(x, dx)
    # These are the initial conditions on the variational equations (the identity matrix)
    ic_var = np.eye(6).reshape((36,)).tolist()

    # And compile the Taylor Integrator
    start_time = time.time()
    ta = hey.taylor_adaptive(
        # The ODEs.
        aug_dyn,
        # The initial conditions.
        [1.0, 0.0, 0.0, 0.0, 1.0, 0.0] + ic_var,
        tol=tol,
    )
    ta.pars[0] = 1.0
    print(
        "--- %s seconds --- to build the kepler_variational Taylor integrator"
        % (time.time() - start_time)
    )
    if filename is not None:
        with open(filename, "wb") as file:
            pkl.dump(ta, file)
    return ta


def create_cr3bp_variational(
    symbols=["x", "y", "z", "vx", "vy", "vz"], tol=1e-18, filename=None
):
    """Builds the adaptive Taylor integrator for the cr3bp problem
    adding the corresponding variational equations.

    NOTE: The parameter is defined as m2/(m1+m2) where m2 is the mass of the 
          secondary body (placed on the positive x axis)

    Args:
        symbols (list, optional): The list of symbols for the state. Defaults to ["x", "y", "z", "vx", "vy", "vz"].
        tol (float, optional): The tolerance of the Taylor integrator. Defaults to 1e-16.
        filename (string, optional): If not None, willcreate the filename and pickle there the Taylor integrator.

    Returns:
        heyoka.taylor_adaptive: the numerical integrator
    """
    # Create the symbolic variables for the state.
    x = np.array(hey.make_vars(*symbols))
    # This will contain the r.h.s. of the equations
    dx = []
    
    r_1=hey.sqrt((x[0]+hey.par[0])**2+x[1]**2+x[2]**2)
    r_2=hey.sqrt((x[0]-(1-hey.par[0]))**2+x[1]**2+x[2]**2)
    dx.append(x[3])
    dx.append(x[4])
    dx.append(x[5])
    dx.append(2*x[4] + x[0]-(1-hey.par[0])*(x[0]+hey.par[0])/(r_1**3)-hey.par[0]*(x[0]+hey.par[0]-1)/(r_2**3))
    dx.append(-2*x[3] + x[1]-(1-hey.par[0])*x[1]/(r_1**3)-hey.par[0]*x[1]/(r_2**3))
    dx.append(- (1-hey.par[0])/(r_1**3)*x[2]-hey.par[0]/(r_2**3)*x[2])

    # We augment the dynamics with the variational equations
    aug_dyn = _add_variational_equations(x, dx)
    # These are the initial conditions on the variational equations (the identity matrix)
    ic_var = np.eye(6).reshape((36,)).tolist()

    # And compile the Taylor Integrator
    start_time = time.time()
    ta = hey.taylor_adaptive(
        # The ODEs.
        aug_dyn,
        # The initial conditions.
        [1.0, 0.0, 0.0, 0.0, 1.0, 0.0] + ic_var,
        tol=tol,
    )
    ta.pars[0] = 0.01215058560962404
    print(
        "--- %s seconds --- to build the cr3bp_variational Taylor integrator"
        % (time.time() - start_time)
    )
    if filename is not None:
        with open(filename, "wb") as file:
            pkl.dump(ta, file)
    return ta

def create_msrero_variational(symbols=["x", "y", "z", "vx", "vy", "vz"], tol=1e-16, filename=None):
    """
    This function builds the adaptive Taylor integrator for the MSR-ERO trajectory, during the insertion in the
    Earth-Moon DRO orbit.

    NOTE: for heyoka.time=0., the date is 2036-01-09T08:47:04.173900. For changing this, change the Taylor adaptive parameter (currently it defaults to 13156.8660205)

    Args:
        symbols (list, optional): The list of symbols for the state. Defaults to ["x", "y", "z", "vx", "vy", "vz"].
        tol (float, optional): The tolerance of the Taylor integrator. Defaults to 1e-16.
        filename (string, optional): If not None, willcreate the filename and pickle there the Taylor integrator.

    Returns:
        heyoka.taylor_adaptive: the numerical integrator
    """
    # Create the symbolic variables for the state.
    x = np.array(hey.make_vars(*symbols))
    # Create a dummy time variable, to be able to take the partials of the Earth and Moon positions w.r.t. time, later on:
    tm=hey.make_vars("tm")

    #useful constants for the MSR-ERO dynamics construction:
    AU=149597870.7*1e3#m
    day=86400.#s
    #we also need the gravitational parameter of Sun, Earth, Moon
    mu_sun=hey.model.get_vsop2013_mus()[0]*AU**3/day**2#in m**3/s**2
    mu_earth=hey.model.get_elp2000_mus()[0]#in m**3/s**2
    mu_moon=hey.model.get_elp2000_mus()[1]#in m**3/s**2

    #let's extract the Earth and Moon vecors w.r.t. the Sun (these are essential to compute their gravitational perturbations in the spacecraft dynamics later on)
    #ELP returns the Moon-Earth vector:
    moon_earth=[]
    moon_earth+=hey.model.elp2000_cartesian_fk5(time=(tm/86400.+hey.par[0])/36525., thresh=1e-8)
    moon_earth=[e*1e3 for e in moon_earth]
    #VSOP returns the Earth-Moon barycenter:
    earth_moon_barycenter=hey.model.vsop2013_cartesian_icrf(3,time=(tm/86400.+hey.par[0])/365250., thresh=1e-8)[:3]
    earth_moon_barycenter[:3]=[e*AU for e in earth_moon_barycenter[:3]]
    #we compute the Earth-Moon barycenter to Moon, and Earth-Moon barycenter to Earth position vector
    b_to_moon=[e*(mu_earth)/(mu_earth+mu_moon) for e in moon_earth]
    b_to_earth=[-e*(mu_moon)/(mu_earth+mu_moon) for e in moon_earth]
    #we can now build the Moon position vector w.r.t. the Sun:
    moon_vector=[a+b for a,b in zip(b_to_moon,earth_moon_barycenter)]
    #let's derive the Moon velocity from its position
    moon_vector += list(hey.diff_tensors(moon_vector, diff_args=[tm]).jacobian[:,0])
    #we do the same with the Earth position vector w.r.t. the Sun:
    earth_vector=[a+b for a,b in zip(b_to_earth,earth_moon_barycenter)]
    #let's derive the Earth velocity from its position
    earth_vector += list(hey.diff_tensors(earth_vector, diff_args=[tm]).jacobian[:,0])
    #now we need to substitute the dummy variable with the heyoka time, for this, it is essential
    #to do it vectorized, otherwise you have a big slowdown
    all_vector=hey.subs(moon_vector+earth_vector,{"tm":hey.time})
    moon_vector=all_vector[:6]
    earth_vector=all_vector[6:]
    #we compule the Earth and Moon position in compact mode, and also extract their values in t0
#    earth=hey.cfunc(earth_vector,compact_mode=True,vars=[])
#    moon=hey.cfunc(moon_vector,compact_mode=True,vars=[])
#    earth_vector_t0=earth(inputs=[],time=0.)
#    moon_vector_t0=moon(inputs=[],time=0.)

    r=hey.sqrt(x[0]**2+x[1]**2+x[2]**2)
    r_earth=hey.sqrt((x[0]-earth_vector[0])**2+(x[1]-earth_vector[1])**2+(x[2]-earth_vector[2])**2)
    r_moon=hey.sqrt((x[0]-moon_vector[0])**2+(x[1]-moon_vector[1])**2+(x[2]-moon_vector[2])**2)

    # This will contain the r.h.s. of the equations
    dx = []
    dx.append(x[3])
    dx.append(x[4])
    dx.append(x[5])
    dx.append(-mu_sun*x[0]/r**3 - mu_earth*(x[0]-earth_vector[0])/r_earth**3 - mu_moon*(x[0]-moon_vector[0])/r_moon**3)
    dx.append(-mu_sun*x[1]/r**3 - mu_earth*(x[1]-earth_vector[1])/r_earth**3 - mu_moon*(x[1]-moon_vector[1])/r_moon**3)
    dx.append(-mu_sun*x[2]/r**3 - mu_earth*(x[2]-earth_vector[2])/r_earth**3 - mu_moon*(x[2]-moon_vector[2])/r_moon**3)

    # We augment the dynamics with the variational equations
    aug_dyn = _add_variational_equations(x, dx)
    # These are the initial conditions on the variational equations (the identity matrix)
    ic_var = np.eye(6).reshape((36,)).tolist()

    # And compile the Taylor Integrator
    start_time = time.time()
    ta = hey.taylor_adaptive(
        # The ODEs.
        aug_dyn,
        # The initial conditions.
        [1.,0.,0.,0.,1.,0.] + ic_var,
        tol=tol,
        compact_mode=True
    )
    ta.pars[0]=13157.366020531252-0.5 #in days --> this, when added to the JD corresponding to midnight on January 1, 2000
                                        #corresponds to 2036-01-09T08:47:04.173900
    print(
        "--- %s seconds --- to build the cr3bp_variational Taylor integrator"
        % (time.time() - start_time)
    )
    if filename is not None:
        with open(filename, "wb") as file:
            pkl.dump(ta, file)
    return ta

def _propagate_grid_variational(self, t_grid):
    retval = np.zeros((len(t_grid), 42))
    retval[0, :] = self.state
    ic_var = np.eye(6).reshape((36,))
    for i, tf in enumerate(t_grid[1:]):
        self.state[6:] = ic_var
        self.propagate_until(tf)
        retval[i+1, :6] = self.state[:6]
        M = self.state[6:].reshape(6,6)
        Mtot = M@(retval[i][6:].reshape(6,6))
        retval[i+1,6:] = Mtot.reshape((36,))
    self.state[6:] = retval[-1, 6:].reshape((36,))
    return retval
 