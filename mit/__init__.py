from numba.core.errors import NumbaDeprecationWarning, NumbaPendingDeprecationWarning
import os
import warnings
import pickle as pkl
warnings.simplefilter('ignore', category=NumbaDeprecationWarning)

from ._taylor_integrators import create_kepler_variational, create_cr3bp_variational, create_msrero_variational, _propagate_grid_variational
from ._symplectic_utils import invM, invMG
from ._minBu_bu import minBu_bu, minBu_bu_p

# We detect the local path where this __init__.py file is
path = __file__.strip("__init__.py")

# We create the Taylor integrators (only the first time the module is loaded). Units by default non dimensional
# If necessary, modify the units assigning the gravitational parameter as ta_kepler_variational.pars[0] = MU_IN_YOUR_UNITS
if os.path.isfile(path + "kepler_variational.pk"):
    with open(path + "kepler_variational.pk", "rb") as file:
        ta_kepler_variational = pkl.load(file)
else:
    print("Creating the kepler_variational Taylor integrator")
    ta_kepler_variational = create_kepler_variational(
        filename=path + "kepler_variational.pk"
    )
    

# CR3BP: mass ratio by default is Moon/Earth, modify if needed.
if os.path.isfile(path + "cr3bp_variational.pk"):
    with open(path + "cr3bp_variational.pk", "rb") as file:
        ta_cr3bp_variational = pkl.load(file)
else:
    print("Creating the cr3bp_variational Taylor integrator")
    ta_cr3bp_variational = create_cr3bp_variational(
        filename=path + "cr3bp_variational.pk"
    )

# MSR-ERO: t0 reference is 13156.8660205 days, which corresponds to 2036-01-09T08:47:04.173900. To change this, you can change the parameter of the Taylor adaptive
if os.path.isfile(path + "msrero_variational.pk"):
    with open(path + "msrero_variational.pk", "rb") as file:
        ta_msrero_variational = pkl.load(file)
else:
    print("Creating the MSR-ERO Taylor integrator")
    ta_msrero_variational = create_msrero_variational(
        filename=path + "msrero_variational.pk"
    )

# Monkey patch the variational integrator with an stm
ta_cr3bp_variational.propagate_grid_variational = _propagate_grid_variational
ta_kepler_variational.propagate_grid_variational = _propagate_grid_variational
ta_msrero_variational.propagate_grid_variational = _propagate_grid_variational