import numpy as np
from numba import njit

@njit(cache=True)
def invM(M):
    retval = np.zeros((6,6))
    retval[:3,:3] = M[3:,3:].T 
    retval[:3,3:] = -M[:3,3:].T
    retval[3:,:3] = -M[3:,:3].T
    retval[3:,3:] = M[:3,:3].T 
    return retval

@njit(cache=True)
def invMG(M):
    L = np.zeros((6,6))
    L[:3,3:] = np.eye(3)
    L[3:,:3] = -np.eye(3)
    L[0,1] = -2
    L[1,0] = 2
    Li = np.zeros((6,6))
    Li[:3,3:] = -np.eye(3)
    Li[3:,:3] = np.eye(3)
    Li[3,4] = -2
    Li[4,3] = 2
    return Li@M.T@L